import sys

if (len(sys.argv) != 2):
    print('please provide file name as an argument.')
    exit(1)

f = open(sys.argv[1],"r")

lines = f.readlines()

commands = [];

# Get all executed commands with their timestamp
for l in lines:
    p = l.split(' ')
    if len(p) > 2:
        # if it is a command
        if p[2] == ">>>":
            commands.append(dict(time= p[3], command = " ".join(p[4:len(p)])))

prevTime = float(commands[0]['time'])

print('import time')
print('from FoxDot import *')

for c in commands:
    for k, v in c.items():
        if k == 'time':
            # time in miliseconds
            t = float(v)
            print ('time.sleep(' + str(t-prevTime)+')')
            prevTime=t
        if k == 'command':
            print('print("'+v[0:len(v)-1]+'")')
            print(v)
            # time.sleep(k)
