# Original code by  Lucas Samaruga, shared at https://scsynth.org/t/python-supercollider-interaction/3793/5

import threading
import subprocess

sclang_proc = subprocess.Popen(
    ['sclang'],
    stdin=subprocess.PIPE,
    stdout=subprocess.DEVNULL, # sys.stdout
    stderr=subprocess.STDOUT,
    bufsize=1,
    universal_newlines=True,
    start_new_session=True)

# Needs to be running free. A problem is stdout blocking stdin.
threading.Thread(target=lambda: sclang_proc.wait(), daemon=True).start()

def sceval(string):
    if not string.endswith('\n'):
        string += '\n'
    sclang_proc.stdin.write(string)
    sclang_proc.stdin.flush()

sceval('Quarks.install("FoxDot")')
sceval('FoxDot.start')
